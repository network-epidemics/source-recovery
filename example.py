from episrc import *
import numpy as np


g = karate_club()
infection_size = 8
simulation_results = []
for source in g.nodes():
  infected_set = susceptible_infected(g, infection_size, source)
  estimate = sort_by_bayesian_optimality(g, infected_set)
  absolute_rank = estimate.index(source)
  simulation_results.append(absolute_rank)

print(np.bincount(simulation_results))
