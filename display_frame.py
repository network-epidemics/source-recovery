import operator as op
import collections as abc
import tkinter as tk
from tkinter import ttk
from matplotlib import pyplot as plt
from matplotlib.backends import backend_tkagg as mpl_tk
from episrc import epidemics


_method_names = ['JC', 'RC', 'DC', 'DA', 'BO', 'MFA', 'GE']

class DisplayFrame(tk.Frame):
  
  def __init__(self, parent, epidemic_graph):
    super().__init__(parent)
    self.epidemic_graph = epidemic_graph
    self.epidemic_graph.trace(self._data_updated_handler)

    self.description_frm = DescriptionFrame(self)
    self.visualization_frm = VisualizationFrame(self)
    self.evaluation_frm = EvaluationFrame(self)

    self.description_frm.grid(row=0, column=0, padx=5, pady=(10, 5), sticky=tk.NSEW)
    self.visualization_frm.grid(row=1, column=0, padx=5, pady=(5, 10), sticky=tk.NSEW)
    self.evaluation_frm.grid(row=0, column=1, rowspan=2, padx=(5, 10), pady=10, sticky=tk.NSEW)
    self.refresh()

  def _data_updated_handler(self, *args):
    self.refresh()

  def refresh(self):
    graph = self.epidemic_graph.get_graph()
    infected_nodes = self.epidemic_graph.get_infected_nodes()
    source_node = self.epidemic_graph.get_source_node()
    results = self.epidemic_graph.get_results()
    rel_ranks = self.epidemic_graph.get_rel_ranks()
    self.description_frm.refresh(graph, infected_nodes, source_node)
    self.visualization_frm.refresh(graph, infected_nodes)
    self.evaluation_frm.refresh(results, rel_ranks)


class DescriptionFrame(tk.LabelFrame):

  def __init__(self, parent):
    super().__init__(parent, text='Descriptions', borderwidth=2, relief=tk.GROOVE)
    self.node_count_var = tk.IntVar()
    self.edge_count_var = tk.IntVar()
    self.source_node_lbl_var = tk.StringVar()
    self.infection_size_lbl_var = tk.StringVar()

    node_count_caption_lbl = ttk.Label(self, text='Nodes:')
    node_count_lbl = ttk.Label(self, textvariable=self.node_count_var, width=5)
    edge_count_caption_lbl = ttk.Label(self, text='Edges:')
    edge_count_lbl = ttk.Label(self, textvariable=self.edge_count_var, width=10)
    epicemic_source_caption_lbl = ttk.Label(self, text='Epidemic source:')
    epicemic_source_lbl = ttk.Label(self, textvariable=self.source_node_lbl_var, width=4)
    infection_size_caption_lbl_2 = ttk.Label(self, text='Infection size:')
    infection_size_lbl = ttk.Label(self, textvariable=self.infection_size_lbl_var, width=4)

    node_count_caption_lbl.grid(row=0, column=0, padx=5, pady=(5, 0), sticky=tk.W)
    node_count_lbl.grid(row=0, column=1, padx=(0, 5), pady=(5, 0), sticky=tk.W)
    edge_count_caption_lbl.grid(row=1, column=0, padx=5, pady=0, sticky=tk.W)
    edge_count_lbl.grid(row=1, column=1, padx=(0, 5), pady=0, sticky=tk.W)
    epicemic_source_caption_lbl.grid(row=2, column=0, padx=5, pady=0, sticky=tk.W)
    epicemic_source_lbl.grid(row=2, column=1, padx=(0, 5), pady=0, sticky=tk.W)
    infection_size_caption_lbl_2.grid(row=3, column=0, padx=5, pady=0, sticky=tk.W)
    infection_size_lbl.grid(row=3, column=1, padx=(0, 5), pady=0, sticky=tk.W)

  def refresh(self, graph, infected_nodes, source_node):
    self.node_count_var.set(graph.number_of_nodes())
    self.edge_count_var.set(graph.number_of_edges())
    if infected_nodes:
      self.source_node_lbl_var.set(source_node)
      self.infection_size_lbl_var.set(len(infected_nodes))
    else:
      self.source_node_lbl_var.set('')
      self.infection_size_lbl_var.set('')


class VisualizationFrame(tk.LabelFrame):

  def __init__(self, parent):
    super().__init__(parent, borderwidth=2, relief=tk.GROOVE)
    self.graph_figure, self.graph_axis = plt.subplots(1, 1, figsize=(6, 4))
    self.graph_mpl_canvas = mpl_tk.FigureCanvasTkAgg(self.graph_figure, self)
    self.graph_mpl_canvas.get_tk_widget().grid(row=0, column=0, padx=5, pady=5)

  def refresh(self, graph, infected_nodes):
    self.graph_axis.cla()
    epidemics.plot(graph, infected_nodes, self.graph_axis)
    self.graph_mpl_canvas.draw()


class EvaluationFrame(tk.LabelFrame):

  def __init__(self, parent):
    super().__init__(parent, text='Evaluations', borderwidth=2, relief=tk.GROOVE)
    irregular_row_header_map = {0: '', 11: 'Rank loss'}
    irregular_postfix_map = {1: 'st', 2: 'nd', 3: 'rd'}
    pady_map = {0: (5, 0), 11: (0, 5)}
    for row_idx in range(12):
      postfix = irregular_postfix_map.get(row_idx, 'th')
      regular_row_header = text = '{}{}'.format(row_idx, postfix)
      text = irregular_row_header_map.get(row_idx, regular_row_header)
      pady = pady_map.get(row_idx, 0)
      row_header_lbl = ttk.Label(self, text=text, borderwidth=1, relief=tk.RIDGE)
      row_header_lbl.grid(row=row_idx, column=0, padx=(5, 0), pady=pady, sticky=tk.EW)
    self._result_cells = abc.defaultdict(list)
    for method_name in _method_names:
      header_lbl = ttk.Label(self, text=method_name, borderwidth=1, relief=tk.RIDGE)
      self._result_cells[method_name].append(header_lbl)
      for i in range(11):
        cell_lbl = ttk.Label(self, text='', width=5, borderwidth=1, relief=tk.RIDGE)
        self._result_cells[method_name].append(cell_lbl)

  def refresh(self, results, rel_ranks):
    for cell_lbls in self._result_cells.values():
      for cell_lbl in cell_lbls:
        cell_lbl.grid_forget()
    sorted_map = sorted(results.items(), key=op.itemgetter(0))
    for col_idx, (method_name, estimates) in enumerate(sorted_map):
      cell_lbls = self._result_cells[method_name]
      padx = (0, 5) if col_idx == len(results) - 1 else 0
      cell_lbls[0].grid(row=0, column=1 + col_idx, padx=padx, pady=(5, 0), sticky=tk.EW)
      for row_idx, estimate in enumerate(estimates[:10]):
        cell_lbl = cell_lbls[1 + row_idx]
        cell_lbl.config(text=estimate)
        cell_lbl.grid(row=1 + row_idx, column=1 + col_idx, padx=padx, pady=0, sticky=tk.EW)
      cell_lbl = cell_lbls[11]
      cell_lbl.config(text='%.2f' % rel_ranks[method_name])
      cell_lbl.grid(row=11, column=1 + col_idx, padx=padx, pady=(0, 5), sticky=tk.EW)
 