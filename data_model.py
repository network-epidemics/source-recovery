import numpy as np
import networkx as nx
from episrc import epidemics
from episrc.recover import center, eigen, bayes, approx_bayes
import tkinter as tk


class EpidemicGraph:

  def __init__(self):
    self.flag = tk.BooleanVar(value=False)
    self.method_name_map = {
        'BO' : bayes.sort_by_bayesian_optimality,
        'RC' : center.sort_by_rumor_centrality,
        'DC' : center.sort_by_degree_centrality,
        'JC' : center.sort_by_jordan_centrality,
        'DA' : eigen.sort_by_dynamical_age,
        'MFA' : approx_bayes.sort_by_meanfield_approximation,
        'GE' : approx_bayes.sort_by_greedy_elimination,
    }
    self._initialize(0)

  def _initialize(self, level):
    if level <= 0:
      self._graph = nx.from_dict_of_lists({0: []})
    if level <= 1:
      self._infected_nodes = []
      self._source_node = None
    if level <= 2:
      self._results = dict()
      self._rel_ranks = dict()
    self._has_changed()

  def get_graph(self):
    return self._graph

  def get_source_node(self):
    return self._source_node

  def get_infected_nodes(self):
    return self._infected_nodes

  def get_results(self):
    return self._results

  def get_rel_ranks(self):
    return self._rel_ranks

  def random_node(self):
    return np.random.choice(self._graph.nodes())

  def set_graph(self, graph):
    self._graph = graph
    self._initialize(1)

  def run_susceptible_infected(self, infection_size, source_node):
    self._infected_nodes = epidemics.susceptible_infected(self._graph, infection_size, source_node)
    self._source_node = source_node
    self._initialize(2)

  def recover_and_evaluate(self, method_names):
    self._recover(method_names)
    self._evaluate()
    self._has_changed()

  def _recover(self, method_names):
    valid_method_names = list(self.method_name_map.keys())
    if any((name not in valid_method_names for name in method_names)):
      raise ValueError('Valid methods are %s!' % ', '.join(valid_method_names))

    self._results = dict()
    for name in method_names:
      method_func = self.method_name_map[name]
      ordered_nodes = method_func(self._graph, self._infected_nodes)
      self._results[name] = ordered_nodes

  def _evaluate(self):
    if not self._results:
      raise ValueError('No recovery has been performed!')
    self._rel_ranks = dict()
    for name, ordered_nodes in self._results.items():
      infection_size = len(ordered_nodes)
      abs_rank = ordered_nodes.index(self._source_node)
      self._rel_ranks[name] = abs_rank / float(infection_size - 1) if ordered_nodes else None

  def _has_changed(self):
    self.flag.set(not self.flag.get())

  def trace(self, command):
    self.flag.trace('w', command)
