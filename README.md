# Epidemic Source Recovery

episrc is a python module that helps identify the patient zero in an epidemic outbreak on a network of subjects.

Follow these lines to install the dependencies and run the demo file.

```
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
python demo.py
# deactivate
```

### Resources
Kazemitabar, S. Jalil, and Arash A. Amini. "Approximate identification of the optimal epidemic source in complex networks." In International Conference on Network Science, pp. 107-125. Springer, Cham, 2020.
