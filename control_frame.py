import tkinter as tk
from tkinter import ttk
from episrc import net


_star = 'Star'
_regular_tree = 'Regular tree'
_random_tree = 'Random tree'
_karate_club = 'Karate club'
_inhouse_graph_names = (_star, _regular_tree, _random_tree, _karate_club)
_max_allowed_graph_size = 1000


class ControlFrame(tk.Frame):
  
  def __init__(self, parent, epidemic_graph):
    super().__init__(parent)
    prompt_frm = PromptFrame(self)
    graph_frm = GraphFrame(self, epidemic_graph, prompt_frm)
    epidemic_frm = EpidemicFrame(self, epidemic_graph, prompt_frm)
    recovery_frm = RecoveryFrame(self, epidemic_graph, prompt_frm)

    graph_frm.grid(row=0, column=0, padx=0, pady=(0, 5), sticky=tk.NSEW)
    epidemic_frm.grid(row=1, column=0, padx=0, pady=5, sticky=tk.NSEW)
    recovery_frm.grid(row=2, column=0, padx=0, pady=5, sticky=tk.NSEW)
    prompt_frm.grid(row=3, column=0, padx=0, pady=(5, 0), sticky=tk.NSEW)


class GraphFrame(tk.LabelFrame):

  def __init__(self, parent, epidemic_graph, prompt_frm):
    super().__init__(parent, text='Graphs', borderwidth=2, relief=tk.GROOVE)
    self.epidemic_graph, self.prompt_frm = epidemic_graph, prompt_frm

    self.inhouse_graph_name_var = tk.StringVar()
    self.inhouse_graph_param1_var = tk.IntVar(value=2)
    self.inhouse_graph_param2_var = tk.IntVar(value=2)

    load_graph_btn = ttk.Button(self, text='Load from file', command=self._load_graph_btn_handler)
    load_inhouse_graph_btn = ttk.Button(self, text='Load a', command=self._load_inhouse_graph_btn_handler)
    inhouse_graph_names_opt = ttk.OptionMenu(self, self.inhouse_graph_name_var, 'pre-defined graph', *_inhouse_graph_names)
    self.inhouse_graph_name_var.trace('w', self._inhouse_graph_name_opt_handler)
    self.inhouse_graph_param1_caption_lbl = ttk.Label(self, text='n')
    self.inhouse_graph_param1_ety = ttk.Entry(self, textvariable=self.inhouse_graph_param1_var, width=5)
    self.inhouse_graph_param2_caption_lbl = ttk.Label(self, text='k')
    self.inhouse_graph_param2_ety = ttk.Entry(self, textvariable=self.inhouse_graph_param2_var, width=5)

    load_graph_btn.grid(row=0, column=0, columnspan=3, padx=5, pady=5, sticky=tk.EW)
    load_inhouse_graph_btn.grid(row=2, column=0, padx=5, pady=5, sticky=tk.EW)
    inhouse_graph_names_opt.grid(row=2, column=1, columnspan=2, padx=(0, 5), pady=5, sticky=tk.EW)
    self._update_inhouse_graph_param_widget_grids()

  def _update_inhouse_graph_param_widget_grids(self, param1=None, param2=None):
    if param1:
      self.inhouse_graph_param1_caption_lbl.configure(text=param1)
      self.inhouse_graph_param1_var.set(2)
      self.inhouse_graph_param1_caption_lbl.grid(row=3, column=1, padx=5, pady=0, sticky=tk.EW)
      self.inhouse_graph_param1_ety.grid(row=3, column=2, padx=(0, 5), pady=0, sticky=tk.E)
    else:
      self.inhouse_graph_param1_caption_lbl.grid_forget()
      self.inhouse_graph_param1_ety.grid_forget()
    if param2:      
      self.inhouse_graph_param2_caption_lbl.configure(text=param2)
      self.inhouse_graph_param2_var.set(2)
      self.inhouse_graph_param2_caption_lbl.grid(row=4, column=1, padx=5, pady=(0, 5), sticky=tk.EW)
      self.inhouse_graph_param2_ety.grid(row=4, column=2, padx=(0, 5), pady=(0, 5), sticky=tk.E)
    else:
      self.inhouse_graph_param2_caption_lbl.grid_forget()
      self.inhouse_graph_param2_ety.grid_forget()

  def _load_graph_btn_handler(self):
    self.prompt_frm.hide_msg()
    filename = tk.filedialog.askopenfilename(
        title='Select a comma-delimited edge-list file',
        filetypes=[('text files', ('*.txt', '*.csv')), ('all files', '*.*')]
    )
    if not filename:
      return
    err, warn, graph = net.load_from_file(filename)
    if err:
      self.prompt_frm.prompt_msg(error='Failed to load the graph!')
      return
    if graph.number_of_nodes() > _max_allowed_graph_size:
      self.prompt_frm.prompt_msg(error='Graph size exceeds %d!' % _max_allowed_graph_size)
      return
    if warn:
      self.prompt_frm.prompt_msg(warn=warn)
    self.epidemic_graph.set_graph(graph)

  def _load_inhouse_graph_btn_handler(self):
    self.prompt_frm.hide_msg()
    graph_name = self.inhouse_graph_name_var.get()
    if graph_name not in _inhouse_graph_names:
      return
    try:
      if graph_name == _star:
        size = self.inhouse_graph_param1_var.get()
        graph = net.star(size=size)
      elif graph_name == _regular_tree:
        depth = self.inhouse_graph_param1_var.get()
        degree = self.inhouse_graph_param2_var.get()
        graph = net.regular_tree(depth=depth, degree=degree)
      elif graph_name == _random_tree:
        size = self.inhouse_graph_param1_var.get()
        graph = net.random_tree(size=size)
      elif graph_name == _karate_club:
        graph = net.karate_club()
      self.epidemic_graph.set_graph(graph)
    except ValueError as err:
      self.prompt_frm.prompt_msg(error=str(err))

  def _inhouse_graph_name_opt_handler(self, *args):
    self.prompt_frm.hide_msg()
    graph_name = self.inhouse_graph_name_var.get()
    if graph_name == _star:
      self._update_inhouse_graph_param_widget_grids('size')
    elif graph_name == _regular_tree:
      self._update_inhouse_graph_param_widget_grids('depth', 'degree')
    elif graph_name == _random_tree:
      self._update_inhouse_graph_param_widget_grids('size')
    else:
      self.inhouse_graph_param1_caption_lbl.grid_forget()
      self.inhouse_graph_param1_ety.grid_forget()
      self.inhouse_graph_param2_caption_lbl.grid_forget()
      self.inhouse_graph_param2_ety.grid_forget()


class EpidemicFrame(tk.LabelFrame):

  def __init__(self, parent, epidemic_graph, prompt_frm):
    super().__init__(parent, text='Epidemics', borderwidth=2, relief=tk.GROOVE)
    self.epidemic_graph, self.prompt_frm = epidemic_graph, prompt_frm

    self.source_node_var = tk.IntVar(value=0)
    self.infection_size_var = tk.IntVar(value=1)
    
    source_node_caption_lbl = ttk.Label(self, text='Source node:')
    source_node_ety = ttk.Entry(self, textvariable=self.source_node_var, width=5)
    randomize_source_node_btn = ttk.Button(self, text='Randomize', command=self._randomize_source_node_btn_handler)
    infection_size_caption_lbl_1 = ttk.Label(self, text='Infection size:')
    infection_size_ety = ttk.Entry(self, textvariable=self.infection_size_var, width=5)
    launch_epidemics_btn = ttk.Button(self, text='Launch SI epidemics', command=self._launch_epidemics_btn_handler)

    source_node_caption_lbl.grid(row=0, column=0, padx=5, pady=(5, 0), sticky=tk.EW)
    source_node_ety.grid(row=0, column=1, padx=(0, 5), pady=(5, 0), sticky=tk.E)
    randomize_source_node_btn.grid(row=1, column=0, columnspan=2, padx=5, pady=5, sticky=tk.EW)
    infection_size_caption_lbl_1.grid(row=2, column=0, padx=5, pady=(5, 0), sticky=tk.EW)
    infection_size_ety.grid(row=2, column=1, padx=(0, 5), pady=(5, 0), sticky=tk.E)
    launch_epidemics_btn.grid(row=3, column=0, columnspan=2, padx=5, pady=5, sticky=tk.EW)

  def _randomize_source_node_btn_handler(self):
    self.prompt_frm.hide_msg()
    self.source_node_var.set(self.epidemic_graph.random_node())

  def _launch_epidemics_btn_handler(self):
    self.prompt_frm.hide_msg()
    source_node = self.source_node_var.get()
    infection_size = self.infection_size_var.get()
    try:
      self.epidemic_graph.run_susceptible_infected(infection_size, source_node)
    except ValueError as err:
      self.prompt_frm.prompt_msg(error=str(err))

class RecoveryFrame(tk.LabelFrame):

  def __init__(self, parent, epidemic_graph, prompt_frm):
    super().__init__(parent, text='Source recovery', borderwidth=2, relief=tk.GROOVE)
    self.epidemic_graph, self.prompt_frm = epidemic_graph, prompt_frm
    
    self.rc_chbtn_var = tk.BooleanVar(value=True)
    self.jc_chbtn_var = tk.BooleanVar(value=True)
    self.dc_chbtn_var = tk.BooleanVar(value=True)
    self.da_chbtn_var = tk.BooleanVar(value=True)
    self.bo_chbtn_var = tk.BooleanVar(value=True)
    self.mfa_chbtn_var = tk.BooleanVar(value=True)
    self.ge_chbtn_var = tk.BooleanVar(value=True)
    rumor_centrality_chbtn = ttk.Checkbutton(self, text='Rumor centrality', variable=self.rc_chbtn_var)
    jordan_centrality_chbtn = ttk.Checkbutton(self, text='Jordan centrality', variable=self.jc_chbtn_var)
    degree_centrality_chbtn = ttk.Checkbutton(self, text='Degree centrality', variable=self.dc_chbtn_var)
    dynamical_age_chbtn = ttk.Checkbutton(self, text='Dynamical age', variable=self.da_chbtn_var)
    bayesian_optimal_chbtn = ttk.Checkbutton(self, text='Bayesian optimal', variable=self.bo_chbtn_var)
    meanfield_approximation_chbtn = ttk.Checkbutton(self, text='Meanfield approximation', variable=self.mfa_chbtn_var)
    greedy_elimination_chbtn = ttk.Checkbutton(self, text='Greedy elimination', variable=self.ge_chbtn_var)
    recover_source_btn = ttk.Button(self, text='Identify the source', command=self._recover_source_btn_handler)

    rumor_centrality_chbtn.grid(row=0, column=0, padx=5, pady=(0, 5), sticky=tk.EW)
    jordan_centrality_chbtn.grid(row=1, column=0, padx=5, pady=(0, 5), sticky=tk.EW)
    degree_centrality_chbtn.grid(row=2, column=0, padx=5, pady=(0, 5), sticky=tk.EW)
    dynamical_age_chbtn.grid(row=3, column=0, padx=5, pady=(0, 5), sticky=tk.EW)
    bayesian_optimal_chbtn.grid(row=4, column=0, padx=5, pady=(0, 5), sticky=tk.EW)
    meanfield_approximation_chbtn.grid(row=5, column=0, padx=5, pady=(0, 5), sticky=tk.EW)
    greedy_elimination_chbtn.grid(row=6, column=0, padx=5, pady=(0, 5), sticky=tk.EW)
    recover_source_btn.grid(row=10, column=0, padx=5, pady=(0, 5), sticky=tk.EW)

  def _recover_source_btn_handler(self, *args):
    method_names = []
    if self.rc_chbtn_var.get():
      method_names.append('RC')
    if self.jc_chbtn_var.get():
      method_names.append('JC')
    if self.ge_chbtn_var.get():
      method_names.append('DC')
    if self.da_chbtn_var.get():
      method_names.append('DA')
    if self.bo_chbtn_var.get():
      method_names.append('BO')
    if self.mfa_chbtn_var.get():
      method_names.append('MFA')
    if self.ge_chbtn_var.get():
      method_names.append('GE')
    try:
      self.epidemic_graph.recover_and_evaluate(method_names)
    except ValueError as err:
      self.prompt_frm.prompt_msg(error=str(err))

class PromptFrame(tk.Frame):

  def __init__(self, parent):
    super().__init__(parent, borderwidth=0, relief=tk.GROOVE)
    self.error_lbl = ttk.Label(self)

  def prompt_msg(self, error=None, warn=None):
    if error:
      message, color = error, 'red'
    elif warn:
      message, color = warn, 'green'
    else:
      return
    self.error_lbl.configure(text=message, foreground=color)
    self.error_lbl.grid(row=0, column=0, padx=5, pady=0, sticky=tk.EW)      

  def hide_msg(self):
    self.error_lbl.grid_forget()
