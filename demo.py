import tkinter as tk
from tkinter import ttk
from control_frame import ControlFrame
from display_frame import DisplayFrame
from data_model import EpidemicGraph


class Demo(tk.Tk):
  
  def __init__(self):
    super().__init__()
    self.title('Epidemic Source Recovery')
    # self.geometry('600x400')
    # TODO columnconfiugre

    epidemic_graph = EpidemicGraph()
    content = ttk.Frame(self)
    control_frm = ControlFrame(content, epidemic_graph)
    display_frm = DisplayFrame(content, epidemic_graph)

    content.grid(row=0, column=0)
    control_frm.grid(row=0, column=0, padx=(10, 5), pady=10, sticky=tk.NSEW)
    display_frm.grid(row=0, column=1, padx=(0, 0), pady=0, sticky=tk.NSEW)


def main():
  app = Demo()
  app.mainloop()


if __name__ == '__main__':
  main()
