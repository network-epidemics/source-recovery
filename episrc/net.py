import random as rnd
import numpy as np
from scipy import sparse
import networkx as nx


__all__ = ['random_tree', 'regular_tree', 'karate_club', 'star', 'load_from_file']


# Ales Rodionov (2003)
def random_tree(size: int) -> nx.Graph:
  if size < 1:
    raise ValueError('size must be positive!')
  
  adj = sparse.lil_matrix((size, size))
  dst = list(range(size))
  rnd.shuffle(dst)
  src = []
  src.append(dst.pop())
  while len(dst) > 0:
    a = rnd.choice(src)
    b = dst.pop()
    adj[a, b] = adj[b, a] = 1
    src.append(b)
  return nx.from_scipy_sparse_matrix(adj.tocsr())


def regular_tree(depth: int, degree: int) -> nx.Graph:
  if degree < 2:
    raise ValueError('degree must be at least 2!')
  
  if depth < 1:
    raise ValueError("depth must must be positive!")
  
  if degree == 2:
    size = 2 * depth + 1
    path = nx.path_graph(size)
    left_mapping = dict(zip(reversed(range(depth + 1)), 2 * np.arange(depth + 1)))
    right_mapping = dict(zip(range(depth + 1, size), 1 + 2 * np.arange(depth)))
    mapping = {**left_mapping, **right_mapping}
    return nx.relabel_nodes(path, mapping)
  
  size = 1 + degree * ((degree - 1) ** depth - 1) // (degree - 2)
  adj = sparse.lil_matrix((size, size))
  adj[0, range(1, degree + 1)] = adj[range(1, degree + 1), 0] = 1
  start_of_layer = 1
  size_of_layer = degree
  for i in range(depth - 1):
    neighbors = start_of_layer + size_of_layer + np.arange(degree - 1)
    for j in range(size_of_layer):
      adj[start_of_layer + j, neighbors] = adj[neighbors, start_of_layer + j] = 1
      neighbors += (degree - 1)
    start_of_layer += size_of_layer
    size_of_layer *= (degree - 1)
  return nx.from_scipy_sparse_matrix(adj.tocsr())


# def small_world(n, k, p):
#     net = nx.watts_strogatz_graph(n=n, k=k, p=p)
#     if not nx.is_connected(net):
#         raise ValueError("The graph was disconnected. Try again!")
#     return nx.adjacency_matrix(net, nodelist=sorted(net.nodes()))


def karate_club():
  return nx.karate_club_graph()


def star(size: int) -> nx.Graph:
  if size < 1:
    raise ValueError('size must be positive!')
  
  if size == 1:
    return nx.from_dict_of_lists({0: []})
  else:
    return nx.from_edgelist([(0, i) for i in range(1, size)])


def load_from_file(filename: str, original_labels=True, delimiter=',') -> (Exception, str, nx.Graph):
  # nodes are relabled so that the choice of source in GUI becomes simpler.
  try:
    net = nx.read_adjlist(filename, nodetype=int, delimiter=delimiter)
  except Exception as e:
    return e, None, None
  warning = None
  if not nx.is_connected(net):
    net = _largest_component(net)
    warning = 'The largest component of the disconnected graph is loaded.'
  if not original_labels:
    net = _relabel_nodes(net)
  return None, warning, net


def _largest_component(graph: nx.Graph) -> nx.Graph:
  c = max(nx.connected_components(graph), key=len)
  return graph.subgraph(c).copy()


def _relabel_nodes(graph: nx.Graph) -> nx.Graph:
  size = graph.number_of_nodes()
  node_mapping = dict(zip(graph.nodes(), range(size)))
  return nx._relabel_nodes(graph, node_mapping)
