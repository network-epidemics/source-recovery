import networkx as nx
import numpy as np
from scipy import sparse
from .. import helper


__all__ = ['sort_by_rumor_centrality', 'sort_by_jordan_centrality', 'sort_by_degree_centrality']


def sort_by_rumor_centrality(graph, infected_nodes):
  infected_node_to_rc = calc_rumor_centrality(graph, infected_nodes)
  sorted_infected_nodes = helper.sort_key_by_value(infected_node_to_rc)
  return sorted_infected_nodes


def calc_rumor_centrality(graph, infected_nodes):
  subgraph = graph.subgraph(infected_nodes)
  assert nx.is_connected(subgraph), 'The infected path must be connected!'
  if not nx.is_tree(subgraph):
    random_root = np.random.RandomState(seed=13).choice(subgraph.nodes())
    subgraph = nx.bfs_tree(subgraph, random_root).to_undirected()
  return _calc_rumor_centrality_for_tree(subgraph)


def _calc_rumor_centrality_for_tree(graph):
  size = graph.number_of_nodes()
  if size == 1:
    the_node = next(iter(graph.nodes()))
    return {the_node: 0}
  nn_map = _gen_node_neighborhood_map(graph)
  trajectory = []
  cut_size = dict()
  last_rc = 1
  leaves = []
  dag = dict()
  for node, neighbors in nn_map.items():
    if len(neighbors) == 1:
      leaves.append(node)
  while len(leaves) > 1:
    l = leaves.pop()
    trajectory.append(l)
    cut_size[l] = cut_size.get(l, 0) + 1
    last_rc *= cut_size[l]
    dag[l] = single_neighbor = nn_map[l].pop()
    cut_size[single_neighbor] = cut_size.get(single_neighbor, 0) + cut_size[l]
    nn_map[single_neighbor].remove(l)
    if len(nn_map[single_neighbor]) == 1:
      leaves.append(single_neighbor)
  root = leaves.pop()
  rc = dict()
  rc[root] = last_rc
  for node in reversed(trajectory):
    last_rc = rc.get(dag[node], 0)
    rc[node] = last_rc // cut_size[node] * (size - cut_size[node])
  return rc


def _gen_node_neighborhood_map(graph):
  nn_map = dict()
  for node in graph.nodes():
    nn_map[node] = list(graph.neighbors(node)) # try set
  return nn_map


def sort_by_jordan_centrality(graph, infected_nodes):
  infected_node_to_jc = jordan_centrality(graph, infected_nodes)
  sorted_infected_nodes = helper.sort_key_by_value(infected_node_to_jc)
  return sorted_infected_nodes


def jordan_centrality(graph, infected_nodes):
  subgraph = graph.subgraph(infected_nodes)
  distance_double_map_generator = nx.shortest_path_length(graph)
  jc = dict()
  for node, distance_map in distance_double_map_generator:
    centrality = max(distance_map.values())
    tie_breaker = sum(distance_map.values())
    jc[node] = (centrality, tie_breaker)
  return jc


def sort_by_degree_centrality(graph, infected_nodes):
  infected_node_to_dc = degree_centrality(graph, infected_nodes)
  sorted_infected_nodes = helper.sort_key_by_value(infected_node_to_dc, reverse=True)
  return sorted_infected_nodes


def degree_centrality(graph, infected_nodes):
  subgraph = graph.subgraph(infected_nodes)  
  dc = {node: subgraph.degree(node) for node in infected_nodes}
  return dc
