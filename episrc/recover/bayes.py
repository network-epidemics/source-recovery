import numpy as np
import networkx as nx
from .. import helper


__all__ = ['sort_by_bayesian_optimality']


def sort_by_bayesian_optimality(graph, infected_nodes):
  if len(infected_nodes) > 10:
    raise ValueError('Bayesian optimality has NP complexity. Infection size must not exceed 10!')
  infected_node_to_posterior = posterior(graph, infected_nodes)
  sorted_infected_nodes = helper.sort_key_by_value(infected_node_to_posterior, reverse=True)
  return sorted_infected_nodes


def posterior(graph, infected_nodes):
  rho = _likelihood(graph, infected_nodes)
  infection_size = len(infected_nodes)
  post = np.zeros(infection_size)
  for i in range(infection_size):
    logicSub = np.zeros(infection_size, dtype=bool)
    logicSub[i] = True
    ind = _logicalToIndex(logicSub)
    post[i] = rho[ind]
  post = post / post.sum()
  post[np.isnan(post)] = 0.0
  return dict(zip(infected_nodes, post))


def _likelihood(graph, infected_nodes):
  graph_size = graph.number_of_nodes()
  inf_size = len(infected_nodes)
  if inf_size == graph_size:
    return {node: 1.0 for node in infected_nodes}
  nodes = list(graph.nodes())
  node_to_idx = dict(zip(nodes, range(graph_size)))
  inf_idx = [node_to_idx[node] for node in infected_nodes]
  adj = nx.adjacency_matrix(graph, nodelist=nodes)
  d = 1
  one_map = np.zeros((1, 0), dtype=bool)
  M = np.zeros((2**inf_size, 2**inf_size))
  M[1, 1] = adj[inf_idx[0], :].sum()
  for i in range(1, inf_size):
    one_map = np.vstack([np.hstack([one_map, np.zeros((d, 1), dtype=bool)]), 
                         np.hstack([one_map, np.ones((d, 1), dtype=bool)])])
    d = d * 2
    subadj = adj[inf_idx[:i], inf_idx[i]].todense() #.reshape((-1, 1))
    vol_I_J_new = np.dot(one_map[:, range(i)], subadj).reshape(-1)
    deg_new = adj[inf_idx[i], :].sum()
    vol_I_Ic_addition = deg_new - 2 * vol_I_J_new
    M[range(d), range(d, 2*d)] = -vol_I_J_new
    M[np.ix_(range(d, 2*d), range(d, 2*d))] = M[np.ix_(range(d), range(d))]
    M[d + np.arange(d), range(d, 2*d)] = M[range(d, 2*d), range(d, 2*d)] + vol_I_Ic_addition
    for j in np.where(subadj == 1)[0]:
      row_idx, col_idx = d + np.where(~one_map[:, j])[0], d + np.where(one_map[:, j])[0]
      M[row_idx, col_idx] = M[row_idx, col_idx] - 1
  b = np.zeros((2**inf_size, 1))
  b[2**inf_size-1, 0] = adj[inf_idx, :].sum() - adj[np.ix_(inf_idx, inf_idx)].sum()
  M[0, 0] = 1
  rho = np.linalg.solve(M, b).reshape(-1)
  return rho


def _logicalToIndex(logicSub):
  ind = 0
  for b in reversed(logicSub):
    ind = 2 * ind + int(b)
  return ind
