import operator as op
import numpy as np
import networkx as nx
from .. import helper


__all__ = ['sort_by_meanfield_approximation', 'sort_by_greedy_elimination']

def sort_by_meanfield_approximation(graph, infected_nodes):
    infected_node_to_mfa = meanfield_approximation(graph, infected_nodes)
    sorted_infected_nodes = helper.sort_key_by_value(infected_node_to_mfa, reverse=True)
    return sorted_infected_nodes


def meanfield_approximation(graph, infected_nodes):
    S, z = _S_z(graph, infected_nodes)
    b = np.linalg.solve(S, z).A1
    return dict(zip(infected_nodes, b))


def _S_z(graph, infected_nodes):
  graph_size = graph.number_of_nodes()
  nodes = list(graph.nodes())
  node_to_idx = dict(zip(nodes, range(graph_size)))
  inf_idx = [node_to_idx[node] for node in infected_nodes]
  adj = nx.adjacency_matrix(graph, nodelist=nodes)
  subadj = adj[np.ix_(inf_idx, inf_idx)]

  u = subadj.sum(axis=1)
  v = adj[inf_idx, :].sum(axis=1) - u    

  S = subadj + subadj * subadj + u * u.T
  S -= subadj.multiply(u) + subadj.multiply(u.T)
  np.fill_diagonal(S, 2 * np.diagonal(S))
  z = (u.sum() + 2 * v.sum()) * u - 2 * np.multiply(u, v) + 2 * (subadj * v + u)
  return S, z


def sort_by_greedy_elimination(graph, infected_nodes):
  subgraph = graph.subgraph(infected_nodes).copy()
  deg_OO, deg_OOc = dict(), dict()
  for node in infected_nodes:
    deg_OO[node] = subgraph.degree(node)
    deg_OOc[node] = graph.degree(node) - deg_OO[node]
  vol_OOc = sum(deg_OOc.values())
  remaining_nodes = set(infected_nodes)
  sorted_infected_nodes = list()
  for i in range(len(infected_nodes) - 1):
    cut_vertices = set(nx.articulation_points(subgraph))
    eligible_nodes = remaining_nodes - cut_vertices
    prob_transition = [(u, deg_OO[u]/(vol_OOc + deg_OO[u] - deg_OOc[u])) for u in eligible_nodes]
    next_node, _ = max(prob_transition, key=op.itemgetter(1))
    for v in subgraph.neighbors(next_node):
      deg_OO[v] -= 1
      deg_OOc[v] += 1
    vol_OOc += deg_OO[next_node] - deg_OOc[next_node]
    del deg_OO[next_node]
    del deg_OOc[next_node]
    remaining_nodes.remove(next_node)
    subgraph.remove_node(next_node)
    sorted_infected_nodes.append(next_node)
  sorted_infected_nodes.append(next(iter(remaining_nodes)))
  sorted_infected_nodes.reverse()
  return sorted_infected_nodes
