import networkx as nx
import numpy as np
from .. import helper


__all__ = ['sort_by_dynamical_age']


def sort_by_dynamical_age(graph, infected_nodes):
  infected_node_to_da = dynamical_age(graph, infected_nodes)
  sorted_infected_nodes = helper.sort_key_by_value(infected_node_to_da, reverse=True)
  return sorted_infected_nodes


def dynamical_age(graph, infected_nodes):
  subgraph = graph.subgraph(infected_nodes).copy()
  lap_eig = _largest_laplacian_eigenvalue(subgraph)
  da = dict()
  for node in infected_nodes:
    subgraph.remove_node(node)
    perturbed_lap_eig = _largest_laplacian_eigenvalue(subgraph)
    da[node] = abs(lap_eig - perturbed_lap_eig) / lap_eig
  return da


def _largest_laplacian_eigenvalue(graph):
  if graph.number_of_nodes() == 0:
    return 0
  return nx.linalg.spectrum.laplacian_spectrum(graph).max()
