import numpy as np
import networkx as nx
from matplotlib import pyplot as plt
from . import helper


__all__ = ['susceptible_infected', 'plot']


def susceptible_infected(graph, infection_size, source_node, shuffle_path=True):
  graph_size = graph.number_of_nodes()
  if source_node not in graph.nodes():
    raise ValueError('Source is not among the nodes!')
  if infection_size < 1:
    raise ValueError('Infection size must be positive!')
  if infection_size > graph_size:
    raise ValueError("Infection size should not exceed the size of graph!")
  nodes = list(graph.nodes())
  idx_to_node = dict(zip(range(graph_size), nodes))
  source_idx = nodes.index(source_node)
  adj = nx.adjacency_matrix(graph, nodelist=nodes)
  infected_path_idx = [source_idx]
  for i in range(infection_size - 1):
    pmf = adj[:, infected_path_idx].sum(axis=1).A1
    pmf[infected_path_idx] = 0
    pmf = pmf / pmf.sum()
    boundary_idx = np.where(pmf > 0)[0]
    infected_path_idx.append(np.random.choice(boundary_idx, p=pmf[boundary_idx]))
  if shuffle_path:
    np.random.shuffle(infected_path_idx)
  infected_path = [idx_to_node[idx] for idx in infected_path_idx]
  return infected_path


def plot(graph, infected_nodes, ax=None):
  if not ax:
    fig, ax = plt.subplots(1, 1)
  pos = nx.spring_layout(graph, seed=13)
  nx.draw_networkx(graph, pos, node_color='w', ax=ax)
  nx.draw_networkx_nodes(graph, pos, nodelist=infected_nodes, node_color='y', ax=ax)
  ax.set_axis_off()
