import operator as op


def sort_key_by_value(dictionary, reverse=False):
  sorted_key, _ = zip(*sorted(dictionary.items(), key=op.itemgetter(1), reverse=reverse))  
  return sorted_key
